# Simgrid platform to dot file converter

**sgp2dot** is a [Simgrid](https://simgrid.org) platform to [Graphiz dot file](http://graphviz.org/) converter implemented in Common Lisp. It requires:
- Common Lisp
- [ASDF](https://common-lisp.net/project/asdf/) (Another System Definition Facility)
- [S-XML](https://common-lisp.net/project/s-xml/) (Simple XML parser)

### How to use it ?

- **First solution:** Compile the project using *asdf* with the following call:
    > (asdf:operate :build-op "sgp2dot")
    
	Then, use the dedicated *sgp2dot-cl.lisp* in your CLI. Note that you should 
	adapt the *sgp2dot-cl.lisp* shebang  according to your lisp implementation.

- **Second, and simpler solution:** Load the system using *asdf*, and invoke
	`(sgp2dot:convert <platform-file> <output-dot-file>)`.
