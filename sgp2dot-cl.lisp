#!/usr/bin/sbcl --script

(load "./sgp2dot.lisp")

(if (eq (length sb-ext:*posix-argv*) 3)
    (sgp2dot:convert (second sb-ext:*posix-argv*) (third sb-ext:*posix-argv*))
    (format t "Usage: ./sgp2dot-cl.lisp <platform-file> <output-dot-file>~%"))

