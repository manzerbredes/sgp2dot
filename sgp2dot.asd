
(defsystem "sgp2dot"
    :description "Generate a dot file (graphiz) to the corresponding Simgrid platform"
    :version "0.0.1"
    :depends-on ("s-xml")
    :build-operation "monolithic-concatenate-source-op"
    :build-pathname "sgp2dot"
    :components ((:file "packages")
                 (:file "convert")))
                 
